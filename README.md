# DISCLAIMER
**SpiderWeb is for education/research purposes only. The author takes NO responsibility and/or liability for how you choose to use any of the tools/source code/any files provided.
 The author and anyone affiliated with will not be liable for any losses and/or damages in connection with use of ANY files provided with SpiderWeb.
 By using SpiderWeb or any files included, you understand that you are AGREEING TO USE AT YOUR OWN RISK. Once again SpiderWeb and ALL files included are for EDUCATION and/or RESEARCH purposes ONLY.
 SpiderWeb is ONLY intended to be used on your own pentesting labs, or with explicit consent from the owner of the property being tested.**

# About SpiderWeb
Windows Compatible ONLY Remote Administration Tool (RAT) designed in C# 

## Features & Functionalities
- Upload/Download files to desired location of infected bug
- Persistence
- Snapshots of infected bugs
- Screen Record of infected bugs
- Live Stream infected bug's screen
- Dump Directories
- Start/Stop Applications
- Display msgbox on bug's screen
- BSOD infected bug's computer
- Plays commanded words through cmd outloud 
- Uninstall SpiderWeb
- Display on server all OS info
- Play audio files; .mp3,and .wav files on bug's computer

## Config
SpiderWeb is a Remote Administration Tool, coded in C#, designed for Windows, and managed on/by webserver ONLY. 

Before compiling bnc.cs to .exe, I recommend creating another copy of the source first before compiling updated/configured .cs file. After opening the file you will see a comment on line 37 saying "SERVER CONFIGURATION" and below that you're going to be replacing default info to your personal/work webserver's info.

In order to configure SpiderWeb you must upload the the compiled RAT to the webserver(make sure you configure it first), you must open the .cs file and replace website url, IP, and the compiled .exe rat to server(This allows for any updates, of malware to be used and installed to bugs' computers). 


You can edit/customize bnc.cs as much as you want, doesn't matter to me, but remember only for educational and research purposes only, unless given permission by client. ;)

## Motivation
I've been into computers since as long as I can remember, growing up it became my full time investment, passion, and career. Diving into the cyber security, and the pentesting field, I realized, I absoulutely love it. This project was actually developed a few years back, when I was just getting into malware development. And Developing this project had helped me exceed, improve, and understand a different perspective of security, and development in C#.


